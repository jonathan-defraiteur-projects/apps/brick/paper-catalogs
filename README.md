# Paper Catalogs

A project to display LEGO's catalogs and navigate through years

![](doc/concept.jpg)
_(ref: [Project documentation](doc/README.md))_

## Backend
[Backend documentation](backend/README.md)

## Frontend
[Frontend documentation](frontend/README.md)
