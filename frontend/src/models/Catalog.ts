import { Page, PagePositionType } from "@/models/Page";
import { AbstractUuidModelImpl, UuidModel } from "@/models/UuidModel";

export enum CatalogFormat {
  Medium,
  ShopAtHome,
}

export interface Catalog extends UuidModel {
  year?: number;
  format?: CatalogFormat;
  serialCode?: number;
  localCode?: string;
  previousYear?: string; // UUID
  nextYear?: string; // UUID
  pages: Page[];
  firstCover?: Page;
  lastCover?: Page;
}

export class CatalogImpl extends AbstractUuidModelImpl implements Catalog {
  year?: number;
  format?: CatalogFormat;
  serialCode?: number;
  localCode?: string;
  previousYear?: string;
  nextYear?: string;
  _pages: Page[] = [];

  constructor(values: Catalog = { pages: [] }) {
    super(values);
    this.year = values.year;
    this.format = values.format;
    this.serialCode = values.serialCode;
    this.localCode = values.localCode;
    this.previousYear = values.previousYear;
    this.nextYear = values.nextYear;
    this.pages = values.pages;
  }

  get pages(): Page[] {
    return this._pages;
  }

  set pages(pages: Page[]) {
    this._pages = pages.map((page) => {
      page.catalogId = this.id;
      return page;
    });
  }

  get firstCover(): Page | undefined {
    return this._pages.find(
      (page) => page.positionType === PagePositionType.FirstCover
    );
  }

  get lastCover(): Page | undefined {
    return this._pages.find(
      (page) => page.positionType === PagePositionType.LastCover
    );
  }
}
