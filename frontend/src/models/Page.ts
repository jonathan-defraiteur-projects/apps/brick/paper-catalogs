import { PageScan } from "@/models/PageScan";
import { AbstractUuidModelImpl, UuidModel } from "@/models/UuidModel";

export enum PagePositionType {
  FirstCover,
  Left,
  Right,
  LastCover,
}

export interface Page extends UuidModel {
  catalogId?: string; // UUID
  number?: number;
  positionType?: PagePositionType;
  previous?: string; // UUID
  next?: string; // UUID
  previousYear?: string; // UUID
  nextYear?: string; // UUID
  scans?: PageScan[];
  scan?: PageScan;
}

export class PageImpl extends AbstractUuidModelImpl implements Page {
  id?: string;
  catalogId?: string;
  number?: number;
  positionType?: PagePositionType;
  previous?: string;
  next?: string;
  previousYear?: string;
  nextYear?: string;
  scans?: PageScan[];

  constructor(values?: Page) {
    super(values);
    if (!values) return;

    this.catalogId = values.catalogId;
    this.number = values.number;
    this.positionType = values.positionType;
    this.previous = values.previous;
    this.next = values.next;
    this.previousYear = values.previousYear;
    this.nextYear = values.nextYear;
    this.scans = values.scans;
  }

  get scan(): PageScan | undefined {
    return this.scans && this.scans.length ? this.scans[0] : undefined;
  }
}
