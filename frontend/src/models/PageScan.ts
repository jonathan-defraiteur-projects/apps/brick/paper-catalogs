import { Source } from "@/models/Source";
import { AbstractUuidModelImpl, UuidModel } from "@/models/UuidModel";

export interface PageScan extends UuidModel {
  language?: string;
  url?: string;
  thumbBase64?: string;
  source?: Source;
}

export class PageScanImpl extends AbstractUuidModelImpl implements PageScan {
  language?: string;
  url?: string;
  thumbBase64?: string;
  source?: Source;

  constructor(value?: PageScan) {
    super(value);
    if (!value) return;

    this.language = value.language;
    this.url = value.url;
    this.thumbBase64 = value.thumbBase64;
    this.source = value.source;
  }
}
