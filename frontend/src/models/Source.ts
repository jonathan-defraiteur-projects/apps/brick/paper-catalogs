import { AbstractUuidModelImpl, UuidModel } from "@/models/UuidModel";

export interface Source extends UuidModel {
  name?: string;
  url: string;
}

export class SourceImpl extends AbstractUuidModelImpl implements Source {
  name?: string;
  url = "";

  constructor(values?: Source) {
    super(values);
    if (!values) return;

    this.name = values.name;
    this.url = values.url;
  }
}
