export interface UuidModel {
  id?: string; // UUID
}

export abstract class AbstractUuidModelImpl implements UuidModel {
  id?: string;

  protected constructor(values?: UuidModel) {
    this.id = values?.id;
  }
}
