import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import { Catalog } from "@/models/Catalog";
import db from "@/utils/db";

export interface State {
  fakeDb: Catalog[];
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    fakeDb: db.buildFakeDb(),
  },
  mutations: {},
  actions: {
    async getCatalog({ state }, id: string): Promise<Catalog> {
      const found = state.fakeDb.find((catalog: Catalog) => catalog.id === id);
      if (!found) throw 404;
      return found;
    },
  },
  modules: {},
});

export function useStore(): Store<State> {
  return baseUseStore(key);
}
