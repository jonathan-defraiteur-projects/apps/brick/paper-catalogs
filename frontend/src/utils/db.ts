import { Page, PageImpl, PagePositionType } from "@/models/Page";
import { PageScanImpl } from "@/models/PageScan";
import { Catalog, CatalogFormat, CatalogImpl } from "@/models/Catalog";

export default {
  buildFakePages(year: number, count: number): Page[] {
    const pages: Page[] = [];
    for (let i = 0; i < count; i++) {
      let values: Page = { number: i + 1 };

      const stringNumber = String(values.number).padStart(3, "0");
      values = {
        ...values,
        id: `id-page-${year}-${stringNumber}`,
        previousYear: `id-page-${year - 1}-${stringNumber}`,
        nextYear: `id-page-${year + 1}-${stringNumber}`,
        scans: [
          new PageScanImpl({
            url: `/img/catalog/medium/${year}/${stringNumber}.jpg`,
          }),
        ],
      };

      // position type
      if (i === 0) values.positionType = PagePositionType.FirstCover;
      else if (i === count - 1)
        values.positionType = PagePositionType.LastCover;
      else
        values.positionType =
          i % 2 == 0 ? PagePositionType.Right : PagePositionType.Left;

      // next and previous
      if (i > 0) {
        values.previous = pages[i - 1].id;
        pages[i - 1].next = values.id;
      }

      pages.push(new PageImpl(values));
    }
    return pages;
  },

  buildFakeDb(): Catalog[] {
    return [
      new CatalogImpl({
        id: "id-med-1991-fr",
        year: 1991,
        format: CatalogFormat.Medium,
        nextYear: "id-med-1992-nl",
        pages: this.buildFakePages(1991, 47),
      }),
      new CatalogImpl({
        id: "id-med-1992-nl",
        year: 1992,
        format: CatalogFormat.Medium,
        previousYear: "id-med-1991-fr",
        nextYear: "id-med-1993-nl",
        pages: this.buildFakePages(1992, 51),
      }),
      new CatalogImpl({
        id: "id-med-1993-nl",
        year: 1993,
        format: CatalogFormat.Medium,
        previousYear: "id-med-1992-nl",
        pages: this.buildFakePages(1993, 47),
      }),
    ];
  },
};
